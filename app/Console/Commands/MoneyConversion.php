<?php
/**
 * Created by PhpStorm.
 * User: turganbay
 * Date: 15/12/2018
 * Time: 21:21
 */

namespace App\Console\Commands;

use App\Console\WalletService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class MoneyConversion extends Command
{
    protected function configure()
    {
        $this->setName('wallet:conversion')
            ->setDescription('Вывод текущей суммы баланса по всем валютам в указанной валюте конвертации одной суммой (EUR, USD, RUB, KZT, KGS)')
            ->setHelp('php artisan wallet:conversion KGS')
            ->addArgument('currency', InputArgument::REQUIRED, 'currency');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $currency = $input->getArgument('currency');
        $myMoney = WalletService::getMyMoney();
        $myConversionMoney = WalletService::getConversion($currency, $myMoney, $output);
        $myConversionMoney['Итог'] = array_sum($myConversionMoney);
        WalletService::displayShow($output, $myConversionMoney);
    }
}