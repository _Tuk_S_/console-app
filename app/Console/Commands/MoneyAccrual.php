<?php
/**
 * Created by PhpStorm.
 * User: turganbay
 * Date: 15/12/2018
 * Time: 21:21
 */

namespace App\Console\Commands;

use App\Console\WalletService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class MoneyAccrual extends Command
{
    protected function configure()
    {
        $this->setName('wallet:accrual')
            ->setDescription('Начисления указанной суммы, без конвертации (итоговая сумма не должна быть меньше 0)')
            ->setHelp('php artisan wallet:accrual');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $money = WalletService::setNewSum($helper, $input, $output);
        WalletService::save($money, $output, 'accrual');
    }
}