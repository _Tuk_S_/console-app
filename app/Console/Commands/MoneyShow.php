<?php
/**
 * Created by PhpStorm.
 * User: turganbay
 * Date: 15/12/2018
 * Time: 21:21
 */

namespace App\Console\Commands;

use App\Console\WalletService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class MoneyShow extends Command
{
    protected function configure()
    {
        $this->setName('wallet:show')
            ->setDescription('Вывод результата накоплений по всем валютам, имеющимся в кошельке')
            ->setHelp('php artisan wallet:show');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $myMoney = WalletService::getMyMoney();
        WalletService::displayShow($output, $myMoney);
    }
}