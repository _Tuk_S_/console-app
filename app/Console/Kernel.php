<?php

namespace App\Console;

use App\Console\Commands\MoneyAccrual;
use App\Console\Commands\MoneyConversion;
use App\Console\Commands\MoneyShow;
use App\Console\Commands\MoneyWithdraw;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Symfony\Component\Console\Application;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
    }

    /**
     * @throws \Exception
     */
    protected function commands()
    {
        $app = new Application();
        $app->add(new MoneyAccrual());
        $app->add(new MoneyWithdraw());
        $app->add(new MoneyShow());
        $app->add(new MoneyConversion());
        $app->run();

        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}
