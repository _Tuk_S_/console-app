<?php
/**
 * Created by PhpStorm.
 * User: turganbay
 * Date: 15/12/2018
 * Time: 23:27
 */

namespace App\Console;

use App\Entities\Wallet;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Question\Question;

class WalletService
{
    static $currencies = ['EUR', 'USD', 'RUB', 'KZT', 'KGS'];

    static $myMoneySum = [
        'EUR' => 0,
        'USD' => 0,
        'RUB' => 0,
        'KZT' => 0,
        'KGS' => 0
    ];

    /**
     * @param $money
     * @param $output
     * @param $action
     */
    static function save($money, $output, $action)
    {
        try {
            $wallet = new Wallet($money);
            $wallet->setAction($action);
            $wallet->setDate(new \DateTime());
            \EntityManager::persist($wallet);
            \EntityManager::flush();
            $output->writeln('<info>Операция успешна</info>');
        } catch (\Exception $e) {
            $output->writeln('<error>Ошибка</error>');
        }
    }

    public static function getMyMoney()
    {
//        $wallets = \EntityManager::getRepository(Wallet::class)->findAll();
        $query = \EntityManager::createQuery("SELECT w FROM App\Entities\Wallet w WHERE w.action = 'accrual'");
        $wallets = $query->getResult();
        foreach ($wallets as $wallet) {
            self::$myMoneySum['EUR'] += $wallet->getEur();
            self::$myMoneySum['USD'] += $wallet->getUsd();
            self::$myMoneySum['RUB'] += $wallet->getRub();
            self::$myMoneySum['KZT'] += $wallet->getKzt();
            self::$myMoneySum['KGS'] += $wallet->getKgz();
        }

        $query = \EntityManager::createQuery("SELECT w FROM App\Entities\Wallet w WHERE w.action = 'withdraw'");
        $wallets = $query->getResult();
        foreach ($wallets as $wallet) {
            self::$myMoneySum['EUR'] -= $wallet->getEur();
            self::$myMoneySum['USD'] -= $wallet->getUsd();
            self::$myMoneySum['RUB'] -= $wallet->getRub();
            self::$myMoneySum['KZT'] -= $wallet->getKzt();
            self::$myMoneySum['KGS'] -= $wallet->getKgz();
        }
        return self::$myMoneySum;
    }

    static function setNewSum($helper, $input, $output)
    {
        $money = [];
        foreach (self::$currencies as $currency) {
            $money[$currency] = floatval(self::askConsole($helper, $input, $output, "Укажите сумму в " . $currency . ': ', 0));
        }
        return $money;
    }

    static function askConsole($helper, $input, $output, $ask, $default)
    {
        $question = new Question($ask, $default);
        $currencySum = $helper->ask($input, $output, $question);
        self::validate($currencySum, $output);
        return floatval($currencySum);
    }

    static function validate($sum, $output)
    {
        if (is_numeric($sum)) {
            $output->writeln('<info>' . $sum . '</info>');
        } else {
            $output->writeln('<error>Сумма должна быть числом!</error>');
            exit;
        }
    }

    static function displayShow($output, $money)
    {
        $output->writeln("\n");
        foreach ($money as $currency => $sum) {
            $output->writeln('<info>' . sprintf('| %4s | %15f |', $currency, $sum) .'</info>');
        }
    }

    static function canWithdrawFromMyAccount($money, $myMoney, $output)
    {
        $check = [];
        foreach (self::$currencies as $currency) {
            if ($money[$currency] > $myMoney[$currency]) {
                $check[$currency] = $money[$currency];
            }
        }
        if (count($check)) {
            $errors = "Недостаточно средств:\n";
            foreach ($check as $currency => $error) {
                $errors .= $error . '-' . $currency . "\n";
            }
            $output->writeln('<error>' . $errors . '</error>');
            exit;
        }
        return true;
    }

    public static function getConversion($to, array $myMoney, $output)
    {
        $progressBar = new ProgressBar($output, 100);
        $progressBar->start();

        $result = [];
        foreach ($myMoney as $from => $sum) {
            if ($from != $to) {
                $currency = file_get_contents("https://www.amadeus.net/api/currencyrate?from={$from}&to={$to}");
                $progressBar->advance(20);
                sleep(3);
                $currency = json_decode($currency, true);
                $result[$from] = $sum * $currency['rate'];
            } else {
                $result[$from] = $sum;
            }
        }
        $progressBar->finish();
        return $result;
    }
}