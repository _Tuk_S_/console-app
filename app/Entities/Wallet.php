<?php
/**
 * Created by PhpStorm.
 * User: turganbay
 * Date: 15/12/2018
 * Time: 00:49
 */

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="wallets")
 */
class Wallet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $usd;
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $eur;
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $kzt;
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $rub;
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $kgz;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('accrual', 'withdraw')")
     */

    private $action;


    public function __construct(array $money)
    {
        $this->eur = $money['EUR'];
        $this->usd = $money['USD'];
        $this->rub = $money['RUB'];
        $this->kzt = $money['KZT'];
        $this->kgz = $money['KGS'];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUsd()
    {
        return $this->usd;
    }


    /**
     * @return mixed
     */
    public function getEur()
    {
        return $this->eur;
    }


    /**
     * @return mixed
     */
    public function getKzt()
    {
        return $this->kzt;
    }


    /**
     * @return mixed
     */
    public function getRub()
    {
        return $this->rub;
    }


    /**
     * @return mixed
     */
    public function getKgz()
    {
        return $this->kgz;
    }


    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     */
    public function setAction($action): void
    {
        $this->action = $action;
    }


}