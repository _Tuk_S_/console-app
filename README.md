
## Необходимые консольные команды для запуска приложения

У вас должны быть свободными порты 80, 3307 и 9000.

- git clone https://_Tuk_S_@bitbucket.org/_Tuk_S_/console-app.git
- cd console-app/
- docker-compose up -d
- docker ps

## Создайте базу данных

- docker-compose exec db bash
- mysql --user=root --password=secret
- CREATE DATABASE wallet DEFAULT CHARACTER SET utf8;
- GRANT ALL ON wallet.* TO 'user'@'%' IDENTIFIED BY 'secret';
- FLUSH PRIVILEGES;
- EXIT;
- exit

## Найстройте конфигурации

- docker-compose exec app bash
- composer install
- cp .env.example .env
- php artisan 
- php artisan key:generate
- php artisan config:cache
- php artisan config:clear
- php artisan doctrine:schema:create
- exit

## Консольные команды по работе с мультивалютным кошельком

Обязательно расскоментируйте строку 45 "$app->run()" в файле app/Console/Kernel.php, чтобы стали доступными команды symfony-console

- docker-compose exec app bash
- php artisan
- php artisan wallet:accrual
- php artisan wallet:show
- php artisan wallet:withdraw
- php artisan wallet:conversion KGS 


## Консольная команда для теста

- docker-compose exec app bash
- vendor/bin/phpunit

## Можно перейти по адресу

- http://localhost/
- http://localhost/register


